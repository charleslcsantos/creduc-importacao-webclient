var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function(){

  return gulp.src('./src/app/**/*.scss')
  	.pipe(sourcemaps.init())
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(autoprefixer())
  	.pipe(sourcemaps.write())
    .pipe(gulp.dest('./src/app'));
});


gulp.task('default', function() {
  gulp.watch('./src/app/**/*.scss',['sass']);
});
