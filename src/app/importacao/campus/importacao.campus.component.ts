import {
  OnInit,
  Component,
  ViewEncapsulation
} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CampusService } from '../../services/campus.service';
import { AlertService } from '../../services/alert.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'importacao-campus',
  templateUrl: './importacao.campus.component.html',
  styleUrls: ['./importacao.campus.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ImportacaoCampusComponent implements OnInit {
  public listCampus;
  public campusSelecionado;

  constructor(
    private activeModal: NgbActiveModal,
    private campusService: CampusService,
    private alertService: AlertService,
    private dataService: DataService
  ) {}

  public ngOnInit() {
    const usuario = this.dataService.user() ? this.dataService.user().nome.split(' ')[0] : '';
    this.campusService.getAllCampus().then((res) => {
        this.listCampus = res;
        if (this.listCampus.length === 1) {
          this.campusSelecionado = this.listCampus[0];
          this.activeModal.close(this.campusSelecionado);
          this.alertService.alert(`Olá ${usuario}, você só possui o campus ${this.campusSelecionado.nome_fantasia} disponível.`, 'success');
        }
        this.dataService.showLoader(false);
      }, (err) => {
        this.dataService.showLoader(false);
        this.alertService.alert(
          'Não foi possível carregar a lista de campus. Atualize a página, caso o problema continue entre em contato com suporte@creduc.com.br',
          'info');
      }
    );
  }

  public searchCampus = (text$: Observable<string>) => {
    return text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map((term) => {
        if (term.length < 2) {
          return [];
        }
        return this.listCampus.filter(
          (v) => new RegExp(term, 'gi')
          .test(v.nome_fantasia)
        ).splice(0, 10);
      });
  }

  public formatCampusResult = (result) => result.nome_fantasia;

  public selectCampus() {
    if (this.campusSelecionado.id_campus != null ) {
      this.activeModal.close(this.campusSelecionado);
    } else {
      this.alertService.alert('Selecione um campus', 'info');
    }
  }
}
