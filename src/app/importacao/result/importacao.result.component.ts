import {
  OnInit,
  Component,
  ViewEncapsulation,
  Input
} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CampusService } from '../../services/campus.service';

@Component({
  selector: 'importacao-result',
  templateUrl: './importacao.result.component.html',
  styleUrls: ['./importacao.result.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ImportacaoResultComponent implements OnInit {
  @Input() public res: any;
  public titulo: string;
  public erro: string;
  constructor(
    private activeModal: NgbActiveModal,
    private campusService: CampusService
  ) {}

  public ngOnInit() {
    if (this.res) {
      this.titulo = this.res.titulo;
      this.erro = this.res.erro;
    }
  }
}
