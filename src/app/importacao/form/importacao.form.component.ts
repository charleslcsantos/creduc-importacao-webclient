import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import {
  Headers
} from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { FileUploader } from 'ng2-file-upload';

import { HttpService } from '../../services/http.service';
import { DataService } from '../../services/data.service';
import { AlertService } from '../../services/alert.service';
import { ModalService } from '../../services/modal.service';
import { CampusService } from '../../services/campus.service';

import { ImportacaoCampusComponent } from '../campus/importacao.campus.component';
import { ImportacaoResultComponent } from '../result/importacao.result.component';

@Component({
  selector: 'importacao-form',
  templateUrl: './importacao.form.component.html',
  styleUrls: ['./importacao.form.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [ CampusService ]
})

export class ImportacaoFormComponent implements OnInit {
  // @Input() public importacao: ImportacaoModel;
  public campusSelecionado;
  public uploader = new FileUploader('https://integracao.creduc.com.br/api/v1/candidato');
  public usuario: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private httpService: HttpService,
    private modalService: ModalService,
    private dataService: DataService,
    private alertService: AlertService,
    private campusService: CampusService
  ) { }

  public ngOnInit() {
    this.usuario = this.dataService.user() ? this.dataService.user().nome.split(' ')[0] : '';
    this.openModalCampus();
  }

  /**
   * onAfterAddingFile
   * Método para manter apenas um arquivo na fila (uploader.queue[]). Quando adicionar um arquivo e já tiver um arquivo na fila, ele apaga o anterior
   */
  public onAfterAddingFile(u: FileUploader) {
    u.onAfterAddingFile = (f) => {
      if (u.queue.length > 1) {
          u.removeFromQueue(u.queue[0]);
      }
    };
  }

  public openModalCampus() {
    const modalRef = this.modalService.open(
      ImportacaoCampusComponent,
      { keyboard: false, windowClass: 'dark-modal', backdrop: 'static' }
    ).result.then((res) => {
      this.campusSelecionado = res;
      if (this.uploader.queue.length > 1) {
        this.uploader.removeFromQueue(this.uploader.queue[0]);
      }
    });
  }

  public openModalResult(result: any) {
    const modalRef = this.modalService.open(
      ImportacaoResultComponent,
      { keyboard: false, windowClass: 'dark-modal', backdrop: 'static' }
    )
    .componentInstance.res = result;
  }

  public import() {
    this.campusService.import(this.uploader, this.campusSelecionado).then((res) => {
      this.dataService.showLoader(false);
      this.alertService.alert('A importação concluída.', 'success');
      this.openModalResult({
        titulo: 'A importação dos candidatos foi concluída!'
      });
    }, (err) => {
      this.dataService.showLoader(false);
      this.alertService.alert('Não foi possível fazer a importação.', 'warning');
      this.openModalResult({
        titulo: 'Ocorreu um erro na importação!',
        erro: err.toString()
      });
    });
  }

  public logout() {
    this.dataService.removeToken();
    this.router.navigate([this.dataService.getLoginURL()]);
  }
}
