import {
  Component,
  OnInit,
  ViewEncapsulation,
  Output,
  EventEmitter
} from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'importacao',
  // Our list of styles in our component. We may add more to compose many styles together
  styleUrls: [ './importacao.component.css' ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  templateUrl: './importacao.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ImportacaoComponent implements OnInit {
  public localState: any;
  public title: string = 'Importação';

  constructor(
    private dataService: DataService
  ) {}

  public ngOnInit() {
    console.log('ngInit Importacao');
  }

  private asyncDataWithWebpack() {
    // you can also async load mock data with 'es6-promise-loader'
    // you would do this if you don't want the mock-data bundled
    // remember that 'es6-promise-loader' is a promise
    setTimeout(() => {

      System.import('../../assets/mock-data/mock-data.json')
        .then((json) => {
          // console.log('async mockData', json);
          this.localState = json;
        });

    });
  }

}
