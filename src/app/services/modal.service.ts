import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class ModalService {
  private modalSource = new Subject<object>();
  public modal$ = this.modalSource.asObservable();

  private modalInstances = [];
  constructor( private ngModalService: NgbModal) {
    this.modal$.subscribe(
      (arg) => {
        this.modalInstances.push(arg);
      }
    );
  }

  public open(content: any, options?) {
    const reference = this.ngModalService.open(content, options);
    this.modalSource.next(reference);
    return reference;
  }

  public closeAllInstances() {
    if (this.modalInstances.length > 0) {
      this.modalInstances.forEach((modal: NgbModalRef) => {
        modal.close();
      });
      this.modalInstances = [];
    }
  }

  public getAllInstances() {
    return this.modalInstances;
  }
}
