import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { DataService } from './data.service';
import { FileUploader } from 'ng2-file-upload';
import {
  Headers
} from '@angular/http';

@Injectable()
export class CampusService {
  constructor(
    private httpService: HttpService,
    private dataService: DataService
  ) { }

  /**
   * getIdBySlug
   * Busca o id de um campus pelo slug
   */
  public getIdBySlug(slug: string) {
    return this.httpService.get(`/instituicao/${slug}`);
  }

  /**
   * getAllCampus
   * Retorna todos os campus que o usuário pode realizar importação
   */
  public getAllCampus() {
    return this.httpService.secureGet('/usuario/campus');
  }

  /**
   * import
   * Envia o arquivo de importação para o servidor especificando qual instituicao receberá os dados
   * @uploader : arquivo com os candidatos
   * @campus : campus que receberá os dados
   */
  public import(uploader: FileUploader, campus) {
    const params = new FormData();
    const headers = new Headers();
    const file = uploader.queue[0];

    this.dataService.showLoader(true);
    headers.append('id_campus', campus.id_campus);

    params.append('arquivo', file._file, file.file.name);
    return this.httpService.securePost('/candidato', params, headers);
  }
}
