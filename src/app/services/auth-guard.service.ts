import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { HttpService } from './http.service';
import { DataService } from './data.service';
import {
  Router,
  ActivatedRoute,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

@Injectable()
export class AuthGuard {
  constructor(
    private location: Location,
    private router: Router,
    private httpService: HttpService,
    private dataService: DataService
  ) { }
  public canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const endpoint = state.url;

    return true;
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.dataService.showLoader(true);
    if ((route.params.slug) && (this.dataService.instituicao().nome !== route.params.slug)) {
      this.dataService.instituicao({nome: route.params.slug});
    }
    return true;
  }
}
