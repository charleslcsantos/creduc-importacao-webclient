import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { AlertModel } from '../models/alertModel';

@Injectable()
export class AlertService {
  private alertSource = new Subject<object>();
  public alert$ = this.alertSource.asObservable();

  /**
   * Cria um alerta na tela
   * alert
   * @param {string} m : mensagem a ser exibida
   * @param {string} t: tipo de mensagem [success, warning, danger, info]
   */
  public alert(m: string, t: string) {
    const newAlert: AlertModel = {
      message: m,
      type: t
    };
    this.alertSource.next(newAlert);
  }

}
