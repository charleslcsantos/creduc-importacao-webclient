import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ModalService } from './modal.service';
import { DataService } from './data.service';

@Injectable()
export class ExceptionsHandlerService {
  constructor(
    private router: Router,
    private modalService: ModalService,
    private dataService: DataService
  ) {}

  /**
   * requestHandleError
   * Manipulando erros das requisições
   */
  public requestHandleError(err) {
    this.dataService.showLoader(false);
    this.modalService.closeAllInstances();
    if (err.status === 403) {
      this.router.navigate([this.dataService.getLoginURL()]);
    }
    if (err.status === 400) {
      this.router.navigate([this.dataService.getLoginURL()]);
    }
  }
}
