import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class DataService {
  private loaderSource = new Subject<boolean>();
  public loader$ = this.loaderSource.asObservable();

  public token(t?) {
    
    if (t) {
      localStorage.setItem(`${this.instituicao().nome}_token`, JSON.stringify(t));
    }
    return JSON.parse(localStorage.getItem(`${this.instituicao().nome}_token`));
  }

  public removeToken() {
    localStorage.removeItem(`${this.instituicao().nome}_token`);
  }

  /**
   * instituicao
   * Retorna o valor cadastrado no item 'ies' do localStorage
   * Se tiver parametro seta o parametro passado como valor
   * @param i?: instituicao
   */
  public instituicao(i?) {
    if (i) {
      localStorage.setItem('ies', JSON.stringify(i));
    }
    return JSON.parse(localStorage.getItem('ies'));
  }
  /**
   * user
   * Retorna o valor cadastrado no item 'user' do localStorage
   * Se tiver parametro seta o parametro passado como valor
   * @param u?: user
   */
  public user(u?) {
    if (u) {
      localStorage.setItem('user', JSON.stringify(u));
    }
    return JSON.parse(localStorage.getItem('user'));
  }

  public getLoginURL() {
    return `${this.instituicao().nome}/login`;
  }

  public showLoader(arg: boolean) {
    this.loaderSource.next(arg);
  }
}
