import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { DataService } from './data.service';
import { ModalService } from './modal.service';
import { ExceptionsHandlerService } from './exceptions-handler.service';
import { Router } from '@angular/router';

@Injectable()
export class HttpService {

  public baseAuthUrl: string = 'production' === ENV ? 'https://integracao.creduc.com.br/auth' : 'https://integracao.creduc.com.br/auth';
  public baseUrl: string = 'production' === ENV ? 'https://integracao.creduc.com.br/api/v1' : 'https://integracao.creduc.com.br/api/v1';
  private regexHttp = /http(s)?:\/\//;

  constructor(
    private http: Http,
    private dataService: DataService,
    private modalService: ModalService,
    private exceptionsHandlerService: ExceptionsHandlerService,
    private router: Router
  ) { }

  public get(endpoint: string, headers?: Headers) {
    if (this.regexHttp.test(endpoint)) {
      return this.http.get(endpoint, {headers});
    } else {
      return this.http.get(this.baseUrl + endpoint, {headers});
    }
  }

  public post(endpoint: string, params, headers?: Headers) {
    if (this.regexHttp.test(endpoint)) {
      return this.http.post(endpoint, params, { headers });
    } else {
      return this.http.post(this.baseUrl + endpoint, params, { headers });
    }
  }

  public secureGet(endpoint: string, updateToken?: boolean) {
    return new Promise((resolve, reject) => {
      let url = this.baseUrl + endpoint;
      if (this.regexHttp.test(endpoint)) {
        url = endpoint;
      }
      this.http.get(url, {headers: this.getHeader()})
      .map((res) => {
        return res.json();
      })
      .subscribe((res) => {
        if ((updateToken) || (updateToken === null) || (updateToken === undefined)) {
          this.refreshToken()
          .then((resToken) => {
            resolve(res);
          }, (err) => {
            this.exceptionsHandlerService.requestHandleError(err);
            reject(err);
          });
        } else {
          resolve(res);
        }
      }, (err) => {
        this.exceptionsHandlerService.requestHandleError(err);
        reject(err);
      });
    });
  }

  public securePost(endpoint: string, params, headers?, updateToken?) {
    return new Promise((resolve, reject) => {
      let url = this.baseUrl + endpoint;
      if (this.regexHttp.test(endpoint)) {
        url = endpoint;
      }
      const options = new RequestOptions({
        headers: this.getHeader(headers)
      });
      this.http.post(url, params, options)
      .map((res) => {
        return res.json();
      })
      .subscribe((res) => {
        if ((updateToken) || (updateToken === null) || (updateToken === undefined)) {
          this.refreshToken()
          .then((resToken) => {
            resolve(res);
          }, (err) => {
            this.exceptionsHandlerService.requestHandleError(err);
            reject(err);
          });
        } else {
          resolve(res);
        }
      }, (err) => {
        this.exceptionsHandlerService.requestHandleError(err);
        reject(err);
      });
    });
  }

  public securePut(endpoint: string, params, headers?, updateToken?) {
    const token = this.dataService.token();
    return new Promise((resolve, reject) => {
      let url = this.baseUrl + endpoint;
      if (this.regexHttp.test(endpoint)) {
        url = endpoint;
      }
      this.http.put(url, params, {headers: this.getHeader(headers)})
      .subscribe((res) => {
        if ((updateToken) || (updateToken === null) || (updateToken === undefined)) {
          this.refreshToken()
          .then((resToken) => {
            resolve(res);
          }, (err) => {
            this.exceptionsHandlerService.requestHandleError(err);
            reject(err);
          });
        } else {
          resolve(res);
        }
      }, (err) => {
        this.exceptionsHandlerService.requestHandleError(err);
        reject(err);
      });
    });
  }

  public secureDelete(endpoint: string, updateToken?) {
    return new Promise((resolve, reject) => {
      this.http.delete(this.baseUrl + endpoint, {headers: this.getHeader()})
      .subscribe((res) => {
        if ((updateToken) || (updateToken === null) || (updateToken === undefined)) {
          this.refreshToken()
          .then((resToken) => {
            resolve(res);
          }, (err) => {
            this.exceptionsHandlerService.requestHandleError(err);
            reject(err);
          });
        } else {
          resolve(res);
        }
      }, (err) => {
        this.exceptionsHandlerService.requestHandleError(err);
        reject(err);
      });
    });
  }

  private refreshToken() {
    const token = this.dataService.token();
    const formData: FormData = new FormData();
    formData.append('access_token', token);

    return new Promise ((resolve, reject) => {
      this.http.post(this.baseAuthUrl + '/refresh', formData)
      .subscribe((res) => {
        const body = JSON.parse(res['_body']);
        this.dataService.token(body.access_token);
        resolve(res);
      }, (err) => {
        this.modalService.closeAllInstances();
        this.router.navigate([this.dataService.getLoginURL()]);
        reject(err);
      });
    });
  }

  private getHeader(headers?): Headers {
    let h;
    const token = this.dataService.token();
    h = new Headers();
    if (headers) {
      h = headers;
    } else {
      h.set('Content-Type', 'application/json');
    }

    try {
      h.set('Authorization', `JWT ${token}`);
    } catch (error) {
      this.modalService.closeAllInstances();
      this.router.navigate([this.dataService.getLoginURL()]);
    }
    return h;
  }
}
