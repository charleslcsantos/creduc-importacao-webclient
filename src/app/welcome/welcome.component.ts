import { Component } from '@angular/core';

@Component({
  selector: 'no-content',
  styleUrls: ['./welcome.css'],
  template: `
    <div class="page-welcome">
      <img src="assets/img/logo-creduc2.png" alt="Creduc - Importação">
      <h1>Creduc - Importação</h1>
      <p>Acesse https://importacao.creduc.com.br/nome_da_instituição</p>
    </div>
  `
})
export class WelcomeComponent {

}
