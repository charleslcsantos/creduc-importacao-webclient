import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
  NgModule,
  ApplicationRef
} from '@angular/core';
import {
  removeNgStyles,
  createNewHosts,
  createInputTransfer
} from '@angularclass/hmr';
import {
  RouterModule,
  PreloadAllModules
} from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FileSelectDirective } from 'ng2-file-upload';
import { ImagePreviewDirective } from './directives/image-upload-preview.directive';
import { TextMaskModule } from 'angular2-text-mask';
import { SweetAlert2Module } from '@toverux/ngsweetalert2';
import { QuillModule } from 'ngx-quill';

/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';
import { HomeComponent } from './home';
// import { ImportacaoComponent } from './importacao';
// import { ImportacaoFormComponent } from './importacao/form/importacao.form.component';
import { ModalComponent } from './components/modal.component';
import { LoginComponent } from './login';
import { NoContentComponent } from './no-content';
import { WelcomeComponent } from './welcome';
import { XLargeDirective } from './home/x-large';
import { ImportacaoComponent } from './importacao/importacao.component';
import { ImportacaoFormComponent } from './importacao/form/importacao.form.component';
import { ImportacaoCampusComponent } from './importacao/campus/importacao.campus.component';
import { ImportacaoResultComponent } from './importacao/result/importacao.result.component';

import { AuthGuard } from './services/auth-guard.service';
import { DataService } from './services/data.service';
import { HttpService } from './services/http.service';
import { AlertService } from './services/alert.service';
import { ModalService } from './services/modal.service';
import { ExceptionsHandlerService } from './services/exceptions-handler.service';
import { CampusService } from './services/campus.service';
import '../styles/styles.scss';
import '../styles/headings.css';

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState
];

interface StoreType {
  state: InternalStateType;
  restoreInputValues: () => void;
  disposeOldHosts: () => void;
}

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ModalComponent,
    NoContentComponent,
    WelcomeComponent,
    ImportacaoComponent,
    ImportacaoFormComponent,
    ImportacaoCampusComponent,
    ImportacaoResultComponent,
    XLargeDirective,
    FileSelectDirective,
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(ROUTES, { useHash: false, preloadingStrategy: PreloadAllModules }),
    TextMaskModule,
    SweetAlert2Module,
    QuillModule
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    ENV_PROVIDERS,
    APP_PROVIDERS,
    AuthGuard,
    HttpService,
    DataService,
    AlertService,
    ModalService,
    ExceptionsHandlerService,
    CampusService
  ],
  entryComponents: [
    ModalComponent,
    ImportacaoCampusComponent,
    ImportacaoResultComponent
  ]
})
export class AppModule {

  constructor(
    public appRef: ApplicationRef,
    public appState: AppState
  ) {}

  public hmrOnInit(store: StoreType) {
    if (!store || !store.state) {
      return;
    }
    console.log('HMR store', JSON.stringify(store, null, 2));
    // set state
    this.appState._state = store.state;
    // set input values
    if ('restoreInputValues' in store) {
      const restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }

    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  public hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);
    // save state
    const state = this.appState._state;
    store.state = state;
    // recreate root elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // save input values
    store.restoreInputValues  = createInputTransfer();
    // remove styles
    removeNgStyles();
  }

  public hmrAfterDestroy(store: StoreType) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }

}
