import {
  Component,
  Input
} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent {
  @Input() public name;
  @Input() public instituicao;

  constructor(public activeModal: NgbActiveModal) {}

}
