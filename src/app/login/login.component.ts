import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import { Http } from '@angular/http';
import { HttpService } from '../services/http.service';
import { CampusService } from '../services/campus.service';
import { DataService } from '../services/data.service';

@Component({
  selector: 'login',
  // Our list of styles in our component. We may add more to compose many styles together
  styleUrls: [ './login.component.css' ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  templateUrl: './login.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [HttpService, DataService, CampusService]
})
export class LoginComponent implements OnInit {
  public params;
  public localState: any;
  public showMsg: boolean;
  public showAnimation: boolean = false;
  public msg = '';
  public auth = {
    username: '',
    password: ''
  };
  public showLoader = false;
  constructor(
    public route: ActivatedRoute,
    private router: Router,
    private httpService: HttpService,
    private campusService: CampusService,
    private dataService: DataService
  ) {}

  public ngOnInit() {
    this.params = this.route.params['_value'];

    this.campusService.getIdBySlug(this.params.slug).subscribe((res) => {
      if (res) {
        const ies = {
          id: res['id'],
          nome: this.params.slug
        };
        this.dataService.instituicao(ies);
      }
    }, (err) => {
      this.router.navigate(['404']);
    });

    this.route
      .data
      .subscribe((data: any) => {
        // your resolved data from route
        this.localState = data.yourData;
      });

    // @TODO colocar metodo da animação no tratamento das rotas
    setTimeout(() => {
      this.showAnimation = true;
    }, 500);

    this.asyncDataWithWebpack();
  }

  public login(event): void {
    event.preventDefault();
    this.showLoader = true;
    this.httpService.post(this.httpService.baseAuthUrl, this.getParams())
    .subscribe((res) => {
      const body = JSON.parse(res['_body']);
      if (body) {
        this.dataService.token(body.access_token);
        this.dataService.user(body.usuario);
        this.router.navigate([`${this.params.slug}/importacao`]);
      }
    },
    (err) => {
      this.msg = err;
      this.showMsg = true;
      this.showLoader = false;
    });
  }

  private getParams() {

    const params = {
      username : this.auth.username,
      password : this.auth.password,
      campus_slug : this.params.slug
    };

    return params;
  }

  private asyncDataWithWebpack() {
    // you can also async load mock data with 'es6-promise-loader'
    // you would do this if you don't want the mock-data bundled
    // remember that 'es6-promise-loader' is a promise
    setTimeout(() => {

      System.import('../../assets/mock-data/mock-data.json')
        .then((json) => {
          // console.log('async mockData', json);
          this.localState = json;
        });

    });
  }

}
