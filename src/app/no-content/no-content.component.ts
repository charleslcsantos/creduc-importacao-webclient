import { Component } from '@angular/core';

@Component({
  selector: 'no-content',
  styleUrls: ['./no-content.css'],
  template: `
    <div class="page-not-found">
      <img src="assets/img/404_error.svg" alt="404 - página não encontrada">
      <h1>Ops..essa página não existe =(</h1>
      <p>A página que você tentou acessar não existe.</p>
      <p>Acesse https://importacao.creduc.com.br/nome_da_instituição</p>
    </div>
  `
})
export class NoContentComponent {

}
