import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { NoContentComponent } from './no-content';
import { WelcomeComponent } from './welcome';
import { AuthGuard } from './services/auth-guard.service';

import { DataResolver } from './app.resolver';

import { ImportacaoComponent } from './importacao/importacao.component';
import { ImportacaoFormComponent } from './importacao/form/importacao.form.component';

export const ROUTES: Routes = [
  { path: '404',    component: NoContentComponent },
  { path: ':slug',  component: HomeComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard],
    children: [
      { path: 'importacao', component: ImportacaoComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard],
        children: [
          { path: '', component: ImportacaoFormComponent, canActivate: [AuthGuard] }
        ]
      }
    ]
  },
  { path: '', component: WelcomeComponent },
  { path: ':slug/login', component: LoginComponent },
  { path: '**',    component: NoContentComponent }
];
