import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import {
  ActivatedRoute,
  Router
} from '@angular/router';

import { AppState } from '../app.service';
import { Title } from './title';
import { XLargeDirective } from './x-large';
import { AlertModel } from '../models/alertModel';
import { HttpService } from '../services/http.service';
import { DataService } from '../services/data.service';
import { AlertService } from '../services/alert.service';

@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'home'
  selector: 'home',  // <home></home>
  // We need to tell Angular's Dependency Injection which providers are in our app.
  providers: [
    Title
  ],
  // Our list of styles in our component. We may add more to compose many styles together
  styleUrls: [ './home.component.css' ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  templateUrl: './home.component.html',
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  public params;
  // Set our default values
  public localState = { value: '' };
  public alerts = Array<AlertModel>();
  public showAnimation: boolean = false;
  public showLoader: boolean = true;

  // TypeScript public modifiers
  constructor(
    private route: ActivatedRoute,
    public appState: AppState,
    public title: Title,
    private httpService: HttpService,
    private router: Router,
    private dataService: DataService,
    private alertService: AlertService
  ) {
    dataService.loader$.subscribe(
      (arg) => {
        this.showLoader = arg;
      }
    );
    alertService.alert$.subscribe(
      (alert: AlertModel) => {
        this.alerts.push(alert);
        // Limitando a exibição de alertas para 3
        if (this.alerts.length === 4) {
          this.alerts.splice(0, 1);
        }
        // Aumentando o tempo de exibição para mensagens com mais de 50 letras
        if (alert.message.length > 50) {
          setTimeout(() => {
            this.alerts.splice(0, 1);
          }, 10000);
        } else {
          setTimeout(() => {
            this.alerts.splice(0, 1);
          }, 5000);
        }
      }
    );
  }

  public ngOnInit() {
    this.params = this.route.params['_value'];

    if ((!this.dataService.token()) || (!this.dataService.token())) {
      this.router.navigate([this.dataService.getLoginURL()]);
      return false;
    }
    this.router.navigate([`${this.params.slug}/importacao`]);
    this.dataService.showLoader(false);

    // @TODO colocar metodo da animação no tratamento das rotas
    setTimeout(() => {
      this.showAnimation = true;
      if (this.router.url === '/') {
        this.router.navigate(['/importacao']);
      }
    }, 500);
  }

  public logout() {
    this.httpService.securePost('/seguranca/logout', {}, false).then((res) => {
      this.router.navigate(['login']);
    }, (err) => {
      console.log(err);
    });
  }

  public submitState(value: string) {
    this.appState.set('value', value);
    this.localState.value = '';
  }

  public closeAlert(alert) {
    const index: number = this.alerts.indexOf(alert);
    this.alerts.splice(index, 1);
  }
}
